import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-default',
  templateUrl: './header-default.component.html',
  styleUrls: ['./header-default.component.scss'],
})
export class HeaderDefaultComponent implements OnInit {
  bgColor = 'bg-white';
  textColor = 'text-black';
  constructor(private router: Router) {}

  ngOnInit(): void {
    if (window.location.pathname === '/work') {
      this.bgColor = 'bg-black';
      this.textColor = 'text-white';
    }
  }
}
