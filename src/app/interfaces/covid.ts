export interface Covid {
  Confirmed?: number;
  Recovered?: number;
  Hospitalized?: number;
  Deaths?: number;
  NewConfirmed?: number;
  NewRecovered?: number;
  NewHospitalized?: number;
  NewDeaths?: number;
  UpdateDate?: string;
  Source?: string;
  DevBy?: string;
  SeverBy?: string;
}
