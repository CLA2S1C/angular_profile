import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Covid } from 'src/app/interfaces/covid';
@Component({
  selector: 'app-covid-page',
  templateUrl: './covid-page.component.html',
  styleUrls: ['./covid-page.component.scss'],
})
export class CovidPageComponent implements OnInit {
  constructor(private api: ApiService) {}
  covid: Covid = {};
  files: any = [];
  imageSrc: any;
  ngOnInit() {
    this.api.todayCovid().subscribe((data: Covid) => {
      this.covid = data;
    });
  }
  onChange(event: any) {
    for (const file of event.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        return this.files.push(reader.result);
      };
      reader.readAsDataURL(file);
    }
  }
}
