import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderDefaultComponent } from './core/interfaces/header-default/header-default.component';
import { FooterDefaultComponent } from './core/interfaces/footer-default/footer-default.component';
import { MenuComponent } from './shared/app-button/menu/menu.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { WorkPageComponent } from './pages/work-page/work-page.component';
import { EducationPageComponent } from './pages/education-page/education-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { CovidPageComponent } from './pages/covid-page/covid-page.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderDefaultComponent,
    FooterDefaultComponent,
    MenuComponent,
    HomePageComponent,
    WorkPageComponent,
    EducationPageComponent,
    AboutPageComponent,
    CovidPageComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
