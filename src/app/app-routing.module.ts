import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { CovidPageComponent } from './pages/covid-page/covid-page.component';
import { EducationPageComponent } from './pages/education-page/education-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { WorkPageComponent } from './pages/work-page/work-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'work', component: WorkPageComponent },
  { path: 'education', component: EducationPageComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'covid', component: CovidPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
