import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Covid } from '../interfaces/covid';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}
  todayCovid(): Observable<Covid> {
    return this.http.get<Covid>('https://covid19.th-stat.com/api/open/today');
  }
}
