import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'my Profile';
  account = { username: 'admin', password: 12345 };
  count = 0;

  getResult() {
    return 'Suchuj';
  }

  onClick() {
    this.count++;
  }
}
